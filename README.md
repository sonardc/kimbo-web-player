# Simple Web Player for kimbo-dsp #

This is part of the kimbo-dsp project  

### Components ###

* VideoJS - http://videojs.com/
* VideoJS playlist plugin by Brightcove - https://github.com/brightcove/videojs-playlist

### Description ###

Reads a directory from the file system and search for video files, then creates a playlist for videojs

### Configuration ###

* Copy the file "includes/config.inc.php.sample" to "includes/config.inc.php"
* Adjust to meet your needs
* Enjoy