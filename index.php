<?php 
// Get the screen resolution of the host
include_once "includes/screenresolution.inc.php"; 
// This creates videojs config and playlist
include_once "includes/playlist.inc.php"; 
?>
<!DOCTYPE html>
<head>
<title>Kimbo Web Player</title>
<meta name="author" content="Jorge Fonseca">
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<link href="assets/css/video-js.min.css" rel="stylesheet">
<link href="assets/css/kimbo.css" rel="stylesheet">
</head>
<body>
  <div class="main_video">
    <!-- Video tag fot video.js. Sources are provided by videojs-playlist -->
    <video id="my-video" class="video-js" 
      data-setup=<?php print $videojs_config; ?> 
    >
    <p class="vjs-no-js">
      To view this video please enable JavaScript, and consider upgrading to a web browser that 
      <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
    </p>
    </video>
  </div>
  <!-- Load video.js after video tag --> 
  <script src="assets/js/video.min.js"></script>
  <!-- Load videojs-playlist plugin --> 
  <script src="assets/js/videojs-playlist.min.js"></script>
  <!-- Playlist and playlist controller --> 
  <script>
<?php 
  print $playlist; 
  print $playlist_control;
?>
  </script>
</body>
