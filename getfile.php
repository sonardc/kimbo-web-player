<?php
// Get the fixed path of the files to gain certain level of security
include_once "includes/config.inc.php";

// Get the requested file
$file = $_GET['file'];

// Check if the file exists
if (file_exists($VPLAYER_URI.DIRECTORY_SEPARATOR.$file)) {
	$type = mime_content_type($VPLAYER_URI.DIRECTORY_SEPARATOR.$file);
	header('Content-Type: ' . $type);
	readfile($VPLAYER_URI.DIRECTORY_SEPARATOR.$file);
}
else {
	print "File Not Found";
}

?>