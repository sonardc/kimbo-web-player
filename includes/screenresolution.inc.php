<?php
// This function user xdpyinfo to get the screen resolution of the host
function screen_size () {
	$xdpyinfo = shell_exec("DISPLAY=:0 xdpyinfo| grep dimensions|awk '{print $2}'");
	$screenxy = explode("x", $xdpyinfo);
	return $screenxy;
}

// Get screen size to the player
$screen_size = screen_size();

// Set variables used on the player
$video_width = trim($screen_size[0]);
$video_height = trim($screen_size[1]);
//$body_style = "width: ".$video_width." px; height: ".$video_height." px;";
?>