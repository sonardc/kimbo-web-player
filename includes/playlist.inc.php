<?php
// Get configuration
include_once "config.inc.php";

// Function to search videos, a better approach
function find_all_files($directory, $extension, $recursive = true) {
    $root = scandir($directory); 
    foreach($root as $value) { 
        if($value === '.' || $value === '..') { continue; } 
        if(is_file("$directory/$value")) { 
            if (is_array($extension)) {
                if (!in_array(strtolower(pathinfo("$directory/$value", PATHINFO_EXTENSION)), $extension, true)) { continue; }
            }
            $result[]="$directory/$value";
            continue; 
        } 
        if($recursive) {
            foreach(find_all_files("$directory/$value", $extension, $recursive) as $value) { 
                $result[]=$value; 
            }
        }
    } 
    return $result; 
} 

// Options for videojs
$videojs_config = <<<EOD
'{ 
    "loadingSpinner": false,
    "controls": false, 
    "autoplay": true, 
    "preload": "none", 
    "loop": false, 
    "width": $video_width, 
    "height": $video_height
}'
EOD;

// Search for files
$files = find_all_files($VPLAYER_URI, $VPLAYER_EXT);

// Shuffle playlist array if that has been configured
if ($playlistmode == "randomize") {
    shuffle ($files);
}

// Set the playlist and controller
$playlist = "var videos = ([\n";
foreach ($files as $video) {
        // Get the file type
        $video_type = mime_content_type($video);
        // Encode URL to workaround the bug in playlist-control.js (posibly related to playlist on video.js)
        $video_file = urlencode(str_replace($VPLAYER_URI.DIRECTORY_SEPARATOR, "", "$video"));
        // Asseble absolute URL workaround the bug in playlist-control.js (posibly related to playlist on video.js)
        $video_script_url = "http://$_SERVER[HTTP_HOST]".str_replace("playlist.php", "", $_SERVER[REQUEST_URI])."getfile.php?file=";
        // Create playlist
        $playlist .= "{ sources: [{ src: '".$video_script_url.$video_file."', type: '".$video_type."' }] },\n";
    }
$playlist .= "]);\n";

$playlist_control = <<<'EOD'
// Sleep function for error handling
function sleepFor( sleepDuration ){
    var now = new Date().getTime();
    while(new Date().getTime() < now + sleepDuration){ /* do nothing */ }
}

// Tell videojs to start playing
var player = videojs('my-video'); 
player.playlist(videos); 

// Playlist controller (play next video, loop the playlist)
player.on('ended', function() { 
    /* console.log('Finished: ' + player.playlist.currentItem()); */
    if (player.playlist.next() === undefined) { 
        /* console.log('Looping playlist'); */
        player.playlist.first(); 
        //player.playlist.currentItem(0); /* This works also */
    };
});

// Error handling. This keep trying every 5 seconds the next video
player.on('error', function() {
        console.log('Error detected, skipping ' + player.playlist.currentItem());
        if (player.playlist.next() === undefined) {
            player.playlist.first();
        }; 
        sleepFor(5000); 
});

EOD;

// // Debugging
// print "<pre>";
// print $videojs_config;
// print $playlist;
// print $playlist_control;
// print "</pre>";
?>